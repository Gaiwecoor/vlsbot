const Augur = require("augurbot"),
  u = require("../utils/utils");

const cities = ["Neverwinter", "Chult"];
const races = ["Human", "Goblin", "Lizardfolk", "Dwarf", "Pterafolk", "Half Elf", "Dragonborn", "Orc"];

const Module = new Augur.Module()
.addCommand({name: "do",
	description: "Do something",
	syntax: "Some Action!",
	aliases: ["act"],
	process: (msg, suffix) => {
    let roll = Math.ceil(Math.random() * 20);
    let dc = Math.ceil(Math.random() * 3) * 5;
    msg.channel.send(`> ${msg.author} attempts to ${suffix} and rolls a **${roll}** against DC ${dc}. ${msg.guild ? msg.member.displayName : msg.author.username} ${roll >= dc ? "succeeds" : "fails"}!`);
  }
})
.addEvent("message", async (msg) => {
  let date = new Date();
  if (
    !msg.author.bot &&
    msg.guild &&
    (
      msg.guild.id == "506654819747102730" &&
      (
        (
          date.getMonth() == 3 &&
          date.getDate() <= 7
        ) ||
        (
          date.getMonth() == 2 &&
          date.getDate() >= 27
        )
      )
    ) &&
    !Module.db.Cities.find({id: msg.author.id}).value()
  ) {
    let userInfo = {
      id: msg.author.id,
      nick: msg.member.nickname || "",
      city: cities[Math.floor(Math.random() * cities.length)],
      race: races[Math.floor(Math.random() * races.length)]
    };

    Module.db.Cities.push(userInfo).write();

    msg.member.setNickname(`${msg.member.displayName} of ${userInfo.city} (${userInfo.race})`)
    .catch(u.noop);
  }
});

module.exports = Module;
