const Augur = require("augurbot"),
  u = require("../utils/utils");

const reacts = {
  ok: "👌",
  error: "⚠"
};

const Module = new Augur.Module()
.addCommand({name: "roll",
	description: "Roll dice",
	syntax: "Dice Expression (e.g. 2d6+3)",
	aliases: ["dice"],
	process: (msg, suffix) => {
    let label;
		if (!suffix) suffix = "1d6";
    else {
      let saved = Module.db.Rolls.find({
        discordId: msg.author.id,
        channelId: msg.channel.id,
        label: suffix.toLowerCase()
      });
      if (saved.value()) {
        suffix = saved.value().exp;
        label = saved.value().label;
      }
    }
		suffix = suffix.toLowerCase().replace(/-/g, "+-").replace(/ /g, "");
		let diceExp = /(\d+)?d\d+(\+-?(\d+)?d?\d+)*/;
		let dice = diceExp.exec(suffix);
		let fateExp = /(\d+)?df(\+-?\d+)?/i;
		let fate = fateExp.exec(suffix);
		if (dice) {
			let exp = dice[0].replace(/\+-/g, "-");
			dice = dice[0].split("+");

			let rolls = [];
			let total = 0;

			dice.forEach((d, di) => {
				rolls[di] = [];
				if (d.includes("d")) {
					let add = (d.startsWith("-") ? -1 : 1);
					if (add == -1) d = d.substr(1);
					if (d.startsWith("d")) d = `1${d}`;
					let exp = d.split("d");
					let num = parseInt(exp[0], 10);
					if (num && num <= 100) {
						for (var i = 0; i < num; i++) {
							let val = Math.ceil(Math.random() * parseInt(exp[1], 10)) * add;
							rolls[di].push((i == 0 ? `**${d}:** ` : "") + val);
							total += val;
						};
					} else {
						msg.reply("I'm not going to roll *that* many dice... 🙄").then(u.clean);
						return;
					}
				} else {
					total += parseInt(d, 10);
					rolls[di].push(`**${d}**`);
				}
			});
			if (rolls.length > 0) {
				let response = `${msg.author} rolled ${exp}${(label ? ` (\`${label}\`)` : "")} and got:\n${total}`
          + ((rolls.reduce((a, c) => a + c.length, 0) > 20) ? "" : ` ( ${rolls.reduce((a, c) => a + c.join(", ") + "; ", "")})`);
				msg.channel.send(response, {split: {maxLength: 1950, char: ","}});
			} else
				msg.reply("you didn't give me anything to roll.").then(u.clean);
		} else if (fate) {
			let exp = fate[0].replace(/\+-/g, "-");
			dice = fate[0].split("+");

			let rolls = [];
			dice.forEach(d => {
				if (d.includes("df")) {
					let add = (d.startsWith("-") ? -1 : 1);
					if (add == -1) d = d.substr(1);
					if (d.startsWith("df")) d = `1${d}`;
					let num = parseInt(d, 10);
					if (num && num <= 100)
						for (var i = 0; i < num; i++) rolls.push( (Math.floor(Math.random() * 3) - 1) * add );
					else {
						msg.reply("I'm not going to roll *that* many dice... 🙄").then(u.clean);
						return;
					}
				} else rolls.push(parseInt(d, 10));
			});
			if (rolls.length > 0) {
				let response = `${msg.author} rolled ${exp}${(label ? ` (\`${label}\`)` : "")} and got:\n${rolls.reduce((c, d) => c + d, 0)}`
          + ((rolls.length > 20) ? "" : ` (${rolls.join(", ")})`);
				msg.channel.send(response, {split: {maxLength: 1950, char: ","}});
			} else
				msg.reply("you didn't give me anything to roll.").then(u.clean);
		} else
			msg.reply("that wasn't a valid dice expression.").then(u.clean);
	}
})
.addCommand({name: "saveroll",
  description: "Save a dice expression",
  syntax: "<Name> <Dice Expression> (e.g. `fireball 5d6+3`)",
  aliases: ["savedice"],
  process: (msg, suffix) => {
    // Multi-channel select
    let channels = [msg.channel];
    if (msg.mentions.channels.size > 0) {
      channels = msg.mentions.channels;
      suffix = suffix.replace(/<#\d+>/g, "").replace(/\s\s+/g, " ").trim();
    }

    suffix = suffix.split(" ");
    let status = "ok";

    if (suffix.length < 1) {
      msg.reply("you need to give me a label and an expression! (e.g. `fireball 5d6+3`)").then(u.clean);
    } else if (suffix.length == 1) {
      // Remove rolls
      channels.forEach(channel => {
        let data = {
          discordId: msg.author.id,
          channelId: channel.id,
          label: suffix[0].toLowerCase()
        };
        if (Module.db.Rolls.find(data).value()) {
          Module.db.Rolls.remove(data).write();
        } else {
          msg.reply(`I couldn't find an entry in ${channel} for \`${suffix[0]}\` to remove!`).then(u.clean);
          status = "error";
        }
      });
    } else {
      // Save Roll
      let label = suffix.shift().toLowerCase();
      let exp = suffix.join(" ");

      channels.forEach(channel => {
        let data = {
          discordId: msg.author.id,
          channelId: channel.id,
          label
        };

        let entry = Module.db.Rolls.find(data);
        if (entry.value()) {
          entry.assign({exp}).write();
        } else {
          data.exp = exp;
          Module.db.Rolls.push(data).write();
        }
      });
    }
    msg.react(reacts[status]);
  }
})
.addCommand({name: "listrolls",
  description: "List your saved dice rolls",
  syntax: "<#channel(s)>",
  aliases: ["rolllist"],
  process: (msg) => {
    // Multi-channel select
    let channels = (msg.mentions.channels.size > 0 ? msg.mentions.channels : [msg.channel]);

    channels.forEach(channel => {
      let rolls = Module.db.Rolls.filter({discordId: msg.author.id, channelId: channel.id});
      if (rolls.size().value() > 0) {
        msg.channel.send(`Rolls saved for **${msg.member ? msg.member.displayName : msg.author.username}** in ${channel}:\n${rolls.value().map(r => `· **${r.label}** (${r.exp})`).join("\n")}`);
      } else {
        msg.channel.send(`No rolls saved for **${msg.member ? msg.member.displayName : msg.author.username}** in ${channel}.`).then(u.clean);
      }
    });
  }
});

module.exports = Module;
