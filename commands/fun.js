const Augur = require("augurbot"),
  GoogleNewsRss = require("google-news-rss"),
  u = require("../utils/utils");

const googleNews = new GoogleNewsRss();

const count = {
  pizza: 0,
  taco: 0
};

const Module = new Augur.Module()
.addCommand({name: "birthday",
  description: "It's your birthday!?",
  syntax: "<@user>", hidden: true,
  category: "Silly",
  process: (msg) => {
    if (msg.mentions.users && msg.mentions.users.size > 0) {
      let birthday = msg.mentions.users.first();
      let flair = [
        ":tada: ",
        ":confetti_ball: ",
        ":birthday: ",
        ":gift: ",
        ":cake: "
      ];
      msg.channel.send(":birthday: :confetti_ball: :tada: Happy Birthday, " + birthday + "! :tada: :confetti_ball: :birthday:").then(() => {
        let birthdayLangs = require("../utils/birthday.json");
        let msgs = birthdayLangs.map(lang => birthday.send(flair[Math.floor(Math.random() * flair.length)] + " " + lang));

        Promise.all(msgs).then(() => {
          birthday.send(":birthday: :confetti_ball: :tada: A very happy birthday to you, from DSM & Dragons! :tada: :confetti_ball: :birthday:").catch(u.noop);
        }).catch(u.noop);
      });
    } else {
      msg.reply("you need to tell me who to celebrate!");
    }
  },
  permissions: (msg) => msg.member && msg.member.hasPermission("MANAGE_GUILD")
})
.addCommand({name: "flex",
  description: "Show it off.",
  category: "Silly",
  process: async (msg) => {
    try {
      const Jimp = require("jimp");

      const arm = "https://cdn.discordapp.com/attachments/488887953939103775/545672817354735636/509442648080121857.png";
      const target = (msg.mentions.users.size > 0 ? msg.mentions.users.first() : msg.author);
      const staticURL = `https://cdn.discordapp.com/avatars/${target.id}/${target.avatar.replace("a_", "")}.png`;

      const right = await Jimp.read(arm);
      const mask = await Jimp.read("./storage/mask.png");
      const avatar = await Jimp.read(staticURL);
      const canvas = new Jimp(368, 128, 0x00000000);

      if (Math.random() > 0.5) right.flip(false, true);
      const left = right.clone().flip(true, (Math.random() > 0.5));

      avatar.resize(128, 128);
      avatar.mask(mask, 0, 0);

      canvas.blit(left, 0, 4);
      canvas.blit(right, 248, 4);

      canvas.blit(avatar, 120, 0);

      await msg.channel.send({files: [await canvas.getBufferAsync(Jimp.MIME_PNG)]});
    } catch(e) { u.alertError(e, msg); }
  }
})
.addCommand({name: "pizzanews",
  description: "Get some pizza news!",
  category: "Silly",
  permissions: (msg) => msg.channel.id == "616052270865580060",
  process: async (msg) => {
    try {
      let news = await googleNews.search("pizza", 5);
      let embed = u.embed().setTitle("Pizza News");
      for (let i = 0; i < Math.min(5, news.length); i++) {
        embed.addField(news[i].title, `${news[i].description}\n[[${news[i].publisher}]](${news[i].link})`);
      }
      msg.channel.send({embed});
    } catch(e) { u.alertError(e, msg); }
  }
})
.addCommand({name: "pizzavtaco",
  category: "Silly",
  permissions: (msg) => msg.channel.id == "616052270865580060",
  aliases: ["pizzavstaco"],
  process: async (msg) => {
    try {
      let description;
      if (count.pizza > count.taco) description = "Pizza is better!";
      else if (count.taco > count.pizza) description = "Taco is better!";
      else description = "We enjoy both equally.";
      let embed = u.embed()
      .setTitle("Pizza vs. Taco")
      .setDescription(description)
      .addField("Pizza", count.pizza, true)
      .addField("Taco", count.taco, true);
      msg.channel.send({embed});
    } catch(e) { u.alertError(e, msg); }
  }
})
.addEvent("message", (msg) => {
  if (msg.channel.id == "616052270865580060" && !msg.author.bot) {
    let react = false;
    if (msg.cleanContent.toLowerCase().includes("pizza") || msg.cleanContent.includes("🍕") || msg.member.displayName.toLowerCase().includes("pizza")) {
      react = msg.react("🍕");
      count.pizza++;
    }
    if (msg.cleanContent.toLowerCase().includes("taco") || msg.cleanContent.includes("🌮")) {
      react = msg.react("🌮");
      count.taco++;
    }
    if (!react) msg.react("❌");
  }
})
.addEvent("messageUpdate", (old, msg) => {
  if (msg.channel.id == "616052270865580060" && !msg.author.bot && !(old.cleanContent.toLowerCase().includes("pizza") || old.cleanContent.includes("🍕")) && (msg.cleanContent.toLowerCase().includes("pizza") || msg.cleanContent.includes("🍕")))
      msg.react("🍕");
});

module.exports = Module;
