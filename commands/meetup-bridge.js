const Augur = require("augurbot"),
  bodyParser = require("body-parser"),
  Express = require("express"),
  config = require("../config/site.json"),
  shortid = require("shortid").generate,
  u = require("../utils/utils"),
  fs = require("fs");

const Meetup = require("../utils/meetup"),
  meetup = new Meetup(config.meetup),
  groupName = "Des-Moines-Dungeons-Dragons-Meetup";

const app = new Express(),
  http = require("http").Server(app);

var guild;

class Session {
  constructor(data) {
    this.title = data.title;
    this.user = data.user;
    this.date = data.date;
    this.time = data.time;
    this.duration = data.duration;
    this.description = data.description;
    this.img = data.img || "https://dsm.vorpallongspear.com/img/dice.jpg";
    this.type = data.type;

    this.channelName = (data.channel.startsWith("#") ? data.channel.substr(1) : data.channel).toLowerCase().replace(/\s+/g, "-");

    this.location = data.location;
    this.attendees = data.attendees;

    this.approved = data.approved;
    this.id = data.id || shortid();
    this.message = data.message;

    this.eventId = data.eventId;
  }

  approve(approve) {
    this.approved = approve;
    return this;
  }

  attach(msg) {
    this.message = msg.id;
    return this;
  }

  embed(published = false) {
    let embed = u.embed().setTimestamp()
    .setTitle(this.title)
    .setDescription(this.description)
    .addField("Time", `${this.date} ${this.time}\n${this.duration} Hours`, true)
    .addField("Location", this.location);
    if (!published) embed.addField("Channel", `${this.type} > ${(this.channel || this.channelName)}`)
    embed.addField("Attendees", this.attendees);

    let member = this.getMember();
    if (member) {
      embed.setAuthor(member.displayName, member.user.displayAvatarURL);
    } else {
      embed.setAuthor(this.user);
    }

    if (this.img) embed.setImage(this.img);
    if (this.eventId) embed.setURL(`https://www.meetup.com/Des-Moines-Dungeons-Dragons-Meetup/events/${this.eventId}/`);

    if (!published) {
      if (this.approved) {
        embed.setColor(0x00ff00)
        .setFooter(`Approved by ${guild.members.get(this.approved).displayName}.`);
      } else if (this.approved === false) {
        embed.setColor(0xff0000)
        .setFooter(`Submission Denied.`);
      } else {
        embed.setFooter("Approve with ✅, deny with 🚫.");
      }
    }

    return embed;
  }

  export() {
    return {
      title: this.title,
      user: this.user,
      date: this.date,
      time: this.time,
      duration: this.duration,
      description: this.description,
      img: this.img,
      type: this.type,
      channel: this.channelName,
      location: this.location,
      attendees: this.attendees,
      approved: this.approved,
      id: this.id,
      message: this.message,
      eventId: this.eventId
    };
  }

  get channel() {
    return guild.channels.get(Module.config.channels[this.type]).children.find(c => c.name.toLowerCase() == this.channelName);
  }

  getMember() {
    let member;

    let [name, disc] = this.user.toLowerCase().split("#");
    const trials = [
      // Exact match
      m => m.user.tag.toLowerCase() == this.user.toLowerCase(),
      // Guild nick
      m => m.displayName.toLowerCase() == name && m.user.discriminator == disc,
      // Nick best match
      m => m.displayName.toLowerCase().startsWith(name) && m.user.discriminator == disc,
      // Username best match
      m => m.user.username.toLowerCase().startsWith(name) && m.user.discriminator == disc,
      // Guild nick, no disc
      m => m.displayName.toLowerCase() == name,
      // Guild nick best match, no disc
      m => m.displayName.toLowerCase().startsWith(name),
      // Username, no disc
      m => m.user.username.toLowerCase() == name,
      // Username best match, no disc
      m => m.user.username.toLowerCase().startsWith(name)
    ];

    for (const trial of trials) {
      let member = guild.members.find(trial);
      if (member) return member;
    }

    return null;
  }

  meetupParams() {
    return {
      description: this.description.substr(0, 50000),
      duration: this.duration * 60 * 60 * 1000,
      event_hosts: "295711693,247897145,280010603,278543263", // Bot, Dank, WhyPsy, Gai
      //featured_photo_id: 29401482,
      name: this.title.substr(0, 80),
      publish_status: "draft",
      rsvp_limit: this.attendees,
      venue_visibility: "members",
      self_rsvp: false,
      time: new Date(this.date + " " + this.time).getTime()
    };
  }

  photoParams() {
    return {
      await: true,
      caption: null,
      photo_album_id: "ID",
      photo: null, // encoded as multipart/form-data
      event_id: this.eventId
    };
  }
}

const Module = new Augur.Module()
.setInit(() => {
  let token = require("../config/meetupToken.json");
  meetup.loadToken(token).setStorage((token) => fs.writeFileSync("./config/meetupToken.json", JSON.stringify(token.export())));

  guild = Module.handler.client.guilds.get(Module.config.guilds.dsm);

  if (!Module.handler.client.shard || Module.handler.client.shard.id == 0) {
    //app.set("views", "./site/views");
    //app.set("view engine", "pug");
    //app.disable("view cache");
    app.use((req, res, next) => {
      res.locals.handler = Module.handler;
      res.locals.bot = Module.handler.client;
      next();
    });

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

    app.get("/startsession", (req, res) => {
      res.redirect("https://forms.gle/9fd1M9xoJ3ACmBzy9");
    });

    app.post("/hook/meetup", async (req, res) => {
      try {
        const channel = guild.channels.get(Module.config.channels.bridgeId);
        const data = req.body;
        const session = new Session(req.body);

        const embed = session.embed();

        const msg = await channel.send({embed});
        await msg.react("✅");
        await msg.react("🚫");

        Module.db.Sessions.push(session.attach(msg).export()).write();

        res.json({status: 200, message: "Success"});
      } catch(error) {
        u.alertError(error, "Meetup Webhook");
        res.status(500).json({status: 500, error});
      }
    });

    app.get("/loginbot", (req, res) => {
      res.redirect(meetup.requestAuthURL(["basic", "event_management"]));
    });

    app.get("/auth/mcb", meetup.requestToken(), (req, res, next) => {
      let token = meetup.token.export();
      fs.writeFileSync("token.json", JSON.stringify(token));
      res.json(meetup.token.export());
    });

    app.use(Express.static("./site/public"));

    // Assume 404'd
    app.use((req, res) => {
      res.status(404).json({status: 404, error: "Page not found."});
    });

    http.listen(config.port, (err) => {
      if (err) console.error(err);
      else console.log("Listening on port", config.port);
    });
  }
})
.setUnload(() => {
  if (!Module.handler.client.shard || Module.handler.client.shard.id == 0) {
    http.close();
  }
  delete require.cache[require.resolve("../utils/meetup")];
})
.addEvent("messageReactionAdd", async (reaction, user) => {
  try {
    const message = reaction.message;
    if (message.channel.id != Module.config.channels.bridgeId) return;
    let sessionData = Module.db.Sessions.find({message: message.id}).value();
    if (sessionData && ((sessionData.approved === null) || (sessionData.approved === undefined)) && !user.bot) {
      let session = new Session(sessionData);
      let member = session.getMember();
      if (reaction.emoji.name == "✅") {
        try {
          message.edit({embed: session.approve(user.id).embed()});
          message.channel.send(`**${message.guild.members.get(user.id).displayName}** approved session **${session.title}**.`);
          //message.channel.send(`Attempting to create an event with the following parameters:\n\`\`\`json\n${JSON.stringify(session.meetupParams(), null, "  ")}\n\`\`\``);
          let posted = await meetup.createEvent(groupName, session.meetupParams());
          session.eventId = posted.id;
          Module.db.Sessions.find({id: session.id}).assign(session.export()).write();

          let channel = session.channel || await guild.createChannel(session.channelName, {type: "text", parent: Module.config.channels[session.type]});
          channel.send((member ? member + " has posted a new session!": "A new session has been posted!"), {embed: session.embed(true)});
        } catch(error) { u.alertError(error, "Meetup Bridge Approve"); }
      } else if (reaction.emoji.name == "🚫") {
        try {
          message.edit({embed: session.approve(false).embed()});
          message.channel.send(`**${message.guild.members.get(user.id).displayName}** rejected session **${session.title}**.`);

          if (member) {
            await message.channel.send(`${user}, what reason should I give for the rejection?`);
            message.channel.awaitMessages(m => m.author.id == user.id, {max: 1, time: 60000}).then(reasons => {
              if (reasons.size == 0) return;
              else reason = reasons.first();
              reason.react("👌");
              member.send(`Sorry, your session **"${session.title}"** was rejected because:\n>>> ${reason.cleanContent}`).catch();
            });
          }
          Module.db.Sessions.find({id: session.id}).assign(session.export()).write();
        } catch(error) { u.alertError(error, "Meetup Bridge Deny"); }
      }
      for (const [reactId, react] of message.reactions) react.remove();
    }
  } catch(error) { u.alertError(error, "Meetup Bridge Reaction"); }
});

module.exports = Module;
