const u = require("../utils/utils"),
  Augur = require("augurbot"),
  request = require("request-promise-native"),
  striptags = require("striptags");

async function schedule(limit = 5) {
  let embed = u.embed()
    .setTitle("Upcoming Meetups")
    .setURL("https://www.meetup.com/Des-Moines-Dungeons-Dragons-Meetup/events/")
    .setTimestamp();

  try {
    const url = "https://api.meetup.com/Des-Moines-Dungeons-Dragons-Meetup/events?has_ended=false&page=" + limit;
    let events = JSON.parse(await request(url));

    embed.setDescription(`The next ${events.length} upcoming DSM & Dragons sessions:`);

    for (let i = 0; i < Math.min(events.length, 25); i++) {
      let event = events[i];
      let description = striptags(event.description);
      if (description.length + event.link.length > 900) description = description.substr(0, description.indexOf(" ", 900 - event.link.length)) + " ...";
      embed.addField(`${event.local_date} - ${event.name} (${event.yes_rsvp_count} / ${event.rsvp_limit})`, `${description}\n[[More Info]](${event.link})`);
    }
  } catch(e) {
    u.alertError(e, "Meetup Fetch Error");
    embed.addField("Error", "Sorry, I ran into an error fetching Meetup details.");
  }
  return embed;
}

const Module = new Augur.Module()
.addCommand({name: "meetups",
	description: "Upcoming Sessions",
	info: "Info for upcoming game sessions.",
	aliases: ["sessions", "meetup", "event", "events"],
  permissions: msg => msg.guild && msg.guild.id == "506654819747102730",
	process: async (msg, suffix) => {
    try {
      let limit = parseInt(suffix, 10) || 5;
      let embed = await schedule(limit);
      msg.channel.send({embed});
    } catch(e) { u.alertError(e, msg); }
	}
})
.setClockwork(() => {
  return setInterval(async function() {
    try {
      let now = new Date();
      if (now.getDay() == 0 && now.getHours() == 12) {
        let embed = await schedule(5);
        // #meetup-events channel
        let channel = Module.handler.client.channels.get("564481569998503977");
        if (channel) await channel.send("Please check Meetup and verify your attendance for events in the coming week!", {embed});
      }
    } catch(e) { u.alertError(e, "Meetup Clockwork"); }
  }, 60 * 60 * 1000);
});

module.exports = Module;
