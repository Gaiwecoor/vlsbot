const Augur = require("augurbot"),
  u = require("../utils/utils");

function userEmbed(member) {
	let roles = member.roles.map(role => role.name);
	let embed = u.embed()
		.setTitle(member.displayName)
		.addField("ID", member.id, true)
		.addField("Joined", member.joinedAt.toUTCString(), true)
		.addField("Account Created", member.user.createdAt.toUTCString(), true)
		.addField("Roles", roles.join(", "), true);

	if (member.user.displayAvatarURL) embed.setThumbnail(member.user.displayAvatarURL);

	return embed;
}

const Module = new Augur.Module()
.addCommand({name: "avatar",
  description: "Get a user's avatar",
  syntax: "[@user]",
  permissions: (msg) => msg.channel.permissionsFor(msg.client.user).has("ATTACH_FILES"),
  process: (msg) => {
    let userMentions = u.userMentions(msg);
    if (!userMentions) userMentions = [msg.author];

    userMentions.forEach(user => {
      if (user.avatarURL) {
        let member = ((msg.guild) ? msg.guild.members.get(user.id) : null);
        let name = (member ? member.displayName : user.username);
        let embed = u.embed()
        .setAuthor(name)
        .setDescription(name + "'s Avatar")
        .setImage(user.avatarURL);
        msg.channel.send({embed: embed});
      } else {
        msg.reply(user + " has not set an avatar.").then(u.clean);
      }
    });
  }
})
.addCommand({name: "info",
  description: "Check when a user joined the server",
  syntax: "[@user]",
  category: "Members",
  process: (msg, suffix) => {
    let users = null;
    let mentions = msg.mentions.users;

    if (!suffix) users = [msg.author];
    else if (mentions) users = mentions;
    else if (suffix) {
      users = suffix.split(/ ?\| ?/);
      if (users.length > 4) { msg.channel.send("Limit of 4 users at once").then(u.clean).catch(console.error); return; }
    }

    users.forEach(user => {
      let member = null;
      if (user.id) {
        member = msg.guild.members.get(user.id);
      } else {
        member = msg.guild.members.find('displayName', user);
      }

      if (member) {
        msg.channel.send({embed: userEmbed(member), disableEveryone: true});
      } else msg.channel.send("User \"" + user + "\" not found");
    });
  },
  permissions: (msg) => msg.guild
})
.addCommand({name: "members",
  description: "How many members are in the server?",
  category: "Members",
  process: (msg) => {
    let	members = msg.guild.members;
    let	online = members.filter(m => m.presence.status != "offline").size;

    let response = `📈 **Members:**\n${msg.guild.memberCount} Members\n${online} Online`;
    msg.channel.send(response).catch(console.error);
  }
});

module.exports = Module;
