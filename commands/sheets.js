const Augur = require("augurbot"),
  u = require("../utils/utils");

const Module = new Augur.Module()
.addCommand({name: "savesheet",
  description: "Save a link to a character sheet.",
  syntax: "Sheet Name https://link.to/sheet",
  process: (msg, suffix) => {
    let linkExp = /https?:\/\/\w+\.\w+\S*/;

    let link = linkExp.exec(suffix);
    let name = suffix.replace(linkExp, "").trim();

    let sheet = Module.db.Sheets.find({discordId: msg.author.id, name});

    if (link) {
      if (sheet.value()) {
        sheet.assign({link: link[0]}).write();
      } else {
        Module.db.Sheets.push({discordId: msg.author.id, name, link: link[0]}).write();
      }
    } else {
      if (sheet.value()) {
        Module.db.Sheets.remove({discordId: msg.author.id, name}).write();
      } else msg.reply("I couldn't find a character sheet by that name.").then(u.clean);
    }
    msg.react("👌");
  }
})
.addCommand({name: "sheet",
  description: "Display a user's character sheet(s).",
  syntax: "[@user] [Sheet Name]",
  process: (msg, suffix) => {
    const target = (msg.mentions.users.size > 0 ? msg.mentions.users.first() : msg.author);
    const name = suffix.replace(/<@!?\d+>/, "").trim();

    const embed = u.embed()
    .setAuthor(target.username, target.displayAvatarURL)
    .setTitle("Character Sheet");

    if (name) {
      let sheet = Module.db.Sheets.find({discordId: target.id, name: name}).value();
      if (sheet) {
        embed.setDescription(`[${sheet.name}](${sheet.link})`);
        msg.channel.send({embed});
      } else msg.reply(`I couldn't find a sheet called ${name} for ${target.username}.`).then(u.clean);
    } else {
      let sheets = Module.db.Sheets.filter({discordId: target.id}).value();
      if (sheets.length > 0) {
        embed.setDescription(sheets.map(sheet => `· [${sheet.name}](${sheet.link})`).join("\n"));
        msg.channel.send({embed});
      } else msg.reply(`I couldn't find any sheets for ${target.username}.`).then(u.clean);
    }
  }
});

module.exports = Module;
