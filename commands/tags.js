const Augur = require("augurbot"),
  u = require("../utils/utils");

async function runTag(msg) {
  try {
    let cmd = await Module.handler.parse(msg);
    if (cmd && Module.db.Tags.find({guildId: msg.guild.id, tag: cmd.command}).value()) {
      let tag = Module.db.Tags.find({guildId: msg.guild.id, tag: cmd.command}).value();
      msg.channel.send(tag.response.replace("<@author>", msg.author).replace("<@target>", msg.mentions.users.first()));
      return true;
    } else if (cmd && (cmd.command == "help") && (Module.db.Tags.filter({guildId: msg.guild.id}).value().length > 0) && !cmd.suffix) {
      let embed = u.embed()
      .setTitle("Custom tags in " + msg.guild.name)
      .setThumbnail(msg.guild.iconURL);

      let prefix = Module.config.prefix.replace(/<@!?d+>/g, `@${msg.guild.members.get(msg.client.user.id).displayName} `);

      let list = Array.from(Module.db.Tags.filter({guildId: msg.guild.id}).value()).map(c => prefix + c.tag).sort();

      embed.setDescription(list.join("\n"));
      msg.author.send(embed);
    }
  } catch(e) { u.alertError(e, msg); }
}

const Module = new Augur.Module()
.addCommand({name: "tag",
  category: "Server Admin",
  syntax: "<Command Name> <Command Response>",
  description: "Adds a custom command for your server.",
  info: "Adds a custom command for your server. If the command has the same name as one of the default commands, the custom command will override the default functionality.",
  process: async (msg, suffix) => {
    try {
      if (suffix) {
        let args = suffix.split(" ");
        let newTag = args.shift().toLowerCase();
        let response = args.join(" ");

        if (response) {
          let cmd = {
            guildId: msg.guild.id,
            tag: newTag,
            response: response
          };

          let oldTag = Module.db.Tags.find({guildId: cmd.guildId, tag: cmd.tag});

          if (oldTag.value()) oldTag.assign({response: response}).write();
          else Module.db.Tags.push(cmd).write();
          msg.react("👌");

        } else if (Module.db.Tags.find({guildId: msg.guild.id, tag: newTag}).value()) {
          Module.db.Tags.remove({guildId: msg.guild.id, tag: newTag}).write();
          msg.react("👌");
        } else
          msg.reply(`I couldn't find the command \`${u.prefix(msg)}${newTag}\` to alter.`);
      } else {
        msg.reply("you need to tell me the command name and the intended command response.").then(u.clean);
      }
    } catch(e) { u.alertError(e, msg); }
  },
  permissions: (msg) => msg.guild && (msg.member.permissions.has("MANAGE_GUILD") || msg.member.permissions.has("ADMINISTRATOR") || Module.config.adminId.includes(msg.author.id))
})
.addEvent("message", async (msg) => {
  if (msg.guild && Module.db.Tags.find({guildId: msg.guild.id})) return await runTag(msg);
})
.addEvent("messageUpdate", async (oldMsg, msg) => {
  if (msg.guild && Module.db.Tags.find({guildId: msg.guild.id})) return await runTag(msg);
});

module.exports = Module;
