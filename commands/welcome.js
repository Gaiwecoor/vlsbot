const Augur = require("augurbot"),
  u = require("../utils/utils");

var r = (parts) => parts[Math.floor(Math.random() * parts.length)];

const Module = new Augur.Module()
.addEvent("guildMemberAdd", async (member) => {
  try {
    if (member.guild.id == Module.config.guilds.dsm) {
      let guild = member.guild;
      let bot = member.client;

      let general = bot.channels.get(Module.config.channels.welcome); // #welcome

      if (!member.user.bot) {
        let welcomeString = "";

        let welcome = [
          "Welcome",
          "Hi there",
          "Glad to have you here",
          "Ahoy"
        ];

        let info1 = [
          "What brings you our way?",
          "How'd you find us?",
          "What games do you play?"
        ];
        welcomeString = `${r(welcome)}, ${member}! ${r(info1)}`;
        general.send(welcomeString);
      }

      if (guild.me.hasPermission("MANAGE_ROLES") && Module.db.Users.has({discordId: member.id, guildId: member.guild.id})) {
        await member.addRoles(Module.db.Users.find({discordId: member.id, guildId: guild.id}).get("roles").value());
      }
    }
  } catch(e) { u.alertError(e, "guildMemberAdd"); }
})
.addEvent("guildMemberUpdate", (oldMember, newMember) => {
  // Update user roles
  if (oldMember.roles.size != newMember.roles.size) {
    let user = Module.db.Users.find({discordId: newMember.id, guildId: newMember.guild.id});
    if (!user.value()) {
      Module.db.Users.push({
        discordId: newMember.id,
        guildId: newMember.guild.id,
        roles: newMember.roles
          .map(r => r.id)
          .filter(r => r != newMember.guild.id)
      }).write();
    } else {
      user.assign({
        roles: newMember.roles
          .map(r => r.id)
          .filter(r => r != newMember.guild.id)
      }).write();
    }
  }
});

module.exports = Module;
