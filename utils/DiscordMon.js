const fs = require("fs");

const stats = ["strength", "dexterity", "defense", "speed"];
var K = 16;

function randBetween(a, b) {
  const min = Math.min(a, b);
  const max = Math.max(a, b);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

class DiscordMon {
  constructor(info) {
    this.name = "";
    this.id = null;
    this.stats = {};
    this.rating = 0;
    this.wins = 0;
    this.battles = 0;
    this.color = 0;

    for (let x in info) {
      if (stats.includes(x)) this.stats[x] = parseInt(info[x], 10);
      else if (x == "bio_name") this.name = info[x];
      else this[x] = info[x];
    }

    for (let s = 0; s < stats.length; s++) {
      const stat = stats[s];
      if (!this.stats[stat]) {
        let sum = this.statSum;
        const r = stats.length - s - 1;
        this.stats[stat] = randBetween(Math.max(22 - sum - (r * 8), 3), Math.min(22 - sum - (r * 3), 8));
      }
    }

    if (!this.avatar) {
      let avatars = fs.readdirSync("./img/avatars").filter(f => f.toLowerCase().endsWith(".png"));
      this.avatar = avatars[Math.floor(Math.random() * avatars.length)];
    }

    return this;
  }

  battle(opponent, stat = null) {
    if (!stat) stat = stats[Math.floor(Math.random() * 4)];

    const ea = this.ea(opponent, stat),
      result = Math.random(),
      margin = Math.abs(ea - result),
      score = (result < ea ? 1 : 0),
      surprise = (((ea < 0.3) && score) || ((ea > 0.7) && !score));

    this.wins += score;
    this.battles++;

    opponent.wins += (1 - score);
    opponent.battles++;

    const xp = parseInt((K * (score - ea)).toFixed(0), 10);

    const gained = (K * 1) + xp;
    const gainedOpponent = (K * 1) - xp;

    this.rating += gained;
    opponent.rating += gainedOpponent;

    return {
      victor: (score ? this : opponent),
      loser: (score ? opponent : this),
      ea, gained, gainedOpponent, margin, surprise,
      type: stat
    };
  }

  ea(opponent, stat = null) {
    const shiftA = (stat ? this.stats[stat] * 20 : 0);
    const shiftB = (stat ? opponent.stats[stat] * 20 : 0);
    const qa = 10 ** ((this.rating + shiftA) / 400);
    const qb = 10 ** ((opponent.rating + shiftB) / 400);
    return (qa / (qa + qb));
  }

  export() {
    return {
      name: this.name,
      id: this.id,
      stats: this.stats,
      rating: this.rating,
      wins: this.wins,
      battles: this.battles,
      avatar: this.avatar,
      color: this.color
    };
  }

  sizeup(opponent) {
    let tally = 0;
    for (let i = 0; i < stats.length; i++) {
      tally += this.ea(opponent, stats[i]);
    }
    return tally / stats.length;
  }

  get statSum() {
    return Object.values(this.stats).reduce((a, c) => a += c, 0);
  }

  theorycraft(opponent, stat = null) {
    if (!stat) stat = stats[Math.floor(Math.random() * 4)];

    const ea = this.ea(opponent, stat);

    const score = (Math.random() < ea ? 1 : 0);

    this.wins += ea;
    this.battles++;

    return this;
  }

  get winrate() {
    return (this.battles ? this.wins / this.battles : 0);
  }
}

module.exports = function(K_INIT = null) {
  if (K_INIT) K = K_INIT;
  return DiscordMon;
};
