const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const adapter = new FileSync("db.json");
const db = low(adapter);

db.defaults({
  cities: [],
  rolls: [],
  sessions: [],
  sheets: [],
  tags: [],
  users: []
}).write();

const Cities = db.get("cities");
const Rolls = db.get("rolls");
const Sessions = db.get("sessions");
const Sheets = db.get("sheets");
const Tags = db.get("tags");
const Users = db.get("users");

module.exports = {
  Cities,
  Rolls,
  Sessions,
  Sheets,
  Tags,
  Users
};
