const request = require("request-promise-native");

class Token {
  constructor(credentials) {
    this.credentials = credentials;
    this.issued = Date.now();
    this.store = () => {};
  }

  async authorize(code) {
    try {
      let options = {
        uri: "https://secure.meetup.com/oauth2/access",
        method: "POST",
        headers: {
          Accept: "application/json"
        },
        json: true,
        form: {
          client_id: this.credentials.client_id,
          client_secret: this.credentials.client_secret,
          grant_type: "authorization_code",
          redirect_uri: this.credentials.redirect_uri,
          code
        }
      };
      let response = await request(options);
      this.setAuthorization(response);
    } catch(error) { throw error; }
    return this;
  }

  get expired() {
    return (Date.now() > (this.issued + this.expires_in * 1000));
  }

  export() {
    return {
      access_token: this.access_token,
      token_type: this.token_type,
      expires_in: this.expires_in,
      refresh_token: this.refresh_token,
      issued: this.issued
    };
  }

  async refresh() {
    if (this.expired) {
      try {
        let options = {
          uri: "https://secure.meetup.com/oauth2/access",
          method: "POST",
          headers: {
            Accept: "application/json",
          },
          json: true,
          form: {
            client_id: this.credentials.client_id,
            client_secret: this.credentials.client_secret,
            grant_type: "refresh_token",
            refresh_token: this.refresh_token
          }
        };
        let response = await request(options);
        this.setAuthorization(response);
      } catch(error) { throw error; }
    }
    return this;
  }

  setAuthorization(token) {
    for (let x in token) {
      this[x] = token[x];
    }
    if (!token.issued) this.issued = Date.now();
    this.store(this);
    return this;
  }

  setStorage(store) {
    this.store = store;
    return this;
  }
}

class Meetup {
  constructor(credentials) {
    this.credentials = credentials;
    this.token = new Token(credentials);
  }

  /*************************
  **  OAuth2 Shenanigans  **
  *************************/
  loadToken(token) {
    this.token.setAuthorization(token);
    return this;
  }

  async refreshToken() {
    try {
      await this.token.refresh();
    } catch(error) { throw error; }
    return this;
  }

  requestAuthURL(scopes = ["basic"], state = null) {
    return (
      "https://secure.meetup.com/oauth2/authorize"
      + "?client_id=" + this.credentials.client_id
      + "&response_type=code"
      + "&redirect_uri=" + this.credentials.redirect_uri
      + "&scope=" + scopes.join("+")
      + "&state=" + state
    );
  }

  requestToken(code) {
    return async (req, res, next) => {
      try {
        code = code || req.query.code;
        await this.token.authorize(code);
        next();
      } catch(error) { res.status(500).json({error}); }
    }
  }

  setStorage(store) {
    this.token.setStorage(store);
    return this;
  }

  /**************************
  **  Internal Mechanisms  **
  **************************/
  call(endpoint, method = "GET", params = {}) {
    return new Promise(async (fulfill, reject) => {
      await this.token.refresh();
      let options = {
        uri: "https://api.meetup.com" + endpoint,
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + this.token.access_token
        },
        json: true,
        method
      };
      if (method == "GET") {
        options.uri += "?" + Object.keys(params).map(key => `${key}=${params[key]}`).join("&");
      } else {
        options.form = params;
      }
      request(options).then(fulfill, reject);
    });
  }

  /****************
  **  API Calls  **
  ****************/

  // Events
  createEvent(groupName, params) {
    return this.call(`/${groupName}/events`, "POST", params);
  }

  getGroupEvent(groupName, eventId) {
    return this.call(`/${groupName}/events/${eventId}`);
  }

  getGroupEvents(groupName, params) {
    return this.call(`/${groupName}/events`, "GET", params);
  }

  // Photos
  getAlbumList(groupName) {
    return this.call(`/${groupName}/photo_albums`);
  }

  // Profiles
  getSelf() {
    return this.call("/members/self");
  }
}

module.exports = Meetup;
