const Discord = require("discord.js"),
  config = require("../config/config.json"),
  fs = require("fs"),
  path = require("path");

const errorLog = new Discord.WebhookClient(config.error.id, config.error.token);

const Utils = {
  alertError: function(error = null, msg = null) {
    if (!error) return;

    let errorInfo = new Discord.RichEmbed()
    .setTimestamp()
    .setTitle(error.name);

    if (typeof msg == "string") {
      errorInfo.addField("Message", msg);
    } else if (msg) {
      let bot = msg.client;
      if (bot.shard) errorInfo.addField("Shard", bot.shard.id, true);

      msg.channel.send("I've run into an error. I've let my owner know.")
        .then(m => m.delete(10000));

      errorInfo
      .addField("User", msg.author.username, true)
      .addField("Location", (msg.guild ? `${msg.guild.name} > ${msg.channel.name}` : "PM"), true)
      .addField("Command", msg.cleanContent, true)
    }

    let errorStack = (error.stack ? error.stack : error.toString());
    if (errorStack.length > 1024) errorStack = errorStack.slice(0, 1000);

    errorInfo.addField("Error", errorStack);

    errorLog.send(errorInfo);

    console.error(Date());
    if (typeof msg == "string") console.error(msg);
    else if (msg) console.error(`${msg.author.username} in ${(msg.guild ? (msg.guild.name + " > " + msg.channel.name) : "DM")}: ${msg.cleanContent}`);
    console.trace(error);
  },
  clean: function(msg, t = 20000) {
    if (msg.deletable && !msg.deleted) msg.delete(t).catch(Utils.alertError);
  },
  embed: () => new Discord.RichEmbed().setColor(config.color),
  errorLog: errorLog,
  noop: () => {},
  userMentions: function(msg) {
    let bot = msg.client;
    let userMentions = msg.mentions.users;
    if (userMentions.has(bot.user.id)) userMentions.delete(bot.user.id);
    return (userMentions.size > 0 ? userMentions : null);
  }
}

module.exports = Utils;
